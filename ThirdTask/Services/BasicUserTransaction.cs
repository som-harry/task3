﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThirdTask.Model;

namespace ThirdTask.Services
{
    class BasicUserTransaction : ITransaction
    {
        public static List<Customer> allCustomer = new List<Customer>();
        private decimal balance = 0;
        static bool userExist;
        private decimal amount = 0; 


        public void CheckAcountBalance()
        {
            CheckUser();
            foreach (var user in allCustomer)
            {
                Console.WriteLine($"Your Account balance is {user.balance}");
            }
            
        }

        public void PayIn()
        {
            //throw new NotImplementedException();
            CheckUser();
            foreach (var user in allCustomer)
            {
                if (userExist)
                {
                    Console.WriteLine("How Much do you want to deposit");
                    decimal.TryParse(Console.ReadLine(), out amount);
                    balance += amount;
                    user.balance = balance;
                    Console.WriteLine($"Your Current account is {user.balance}");
                }
                else
                {
                    Console.WriteLine("You have not yet deposit any money");
                }

            }
               
        }

        public void Transfer()
        {
            throw new NotImplementedException();
        }

        public void WithDraw()
        {
            CheckUser();
            //throw new NotImplementedException();
            foreach (var user in allCustomer)
            {
                if (userExist)
                {
                    Console.WriteLine("How much do you wish to withdraw");
                    decimal.TryParse(Console.ReadLine(), out amount);
                    if (amount <= user.balance)
                    {
                        user.balance -= amount;
                       // user.balance = balance;
                        Console.WriteLine($"your avaliable balance is {user.balance}");

                    }
                    else
                    {
                        Console.WriteLine($"Insuffficient account balance");
                    }
                }
            }

            

        }

        public static void CheckUser()
        {
            Console.WriteLine("Type in Your Account Number");

            var verify = Convert.ToInt32(Console.ReadLine());

            foreach (var user in allCustomer) 
            {
            if (user.AccountNumber == verify )
                {
                    userExist = true;
                    
                }
            else
                {
                    userExist = false;
                }
            }
            
           

        }

    }
}
