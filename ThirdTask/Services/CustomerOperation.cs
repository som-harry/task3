﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ThirdTask.Services
{
    class CustomerOperation
    {
        public static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("please select in the following option ");
            Console.WriteLine("1) To create Account");
            Console.WriteLine("2) To payIn");
            Console.WriteLine("3) To withdraw");
            Console.WriteLine("4) To check your balance");
            Console.WriteLine("5) Exist");

            var result = Console.ReadLine();

            if (result == "1")
            {
                GetCustomer getCustomer = new GetCustomer();
                getCustomer.CreateUser();               
                return true;
            }
            else if (result == "2")
            {
                BasicUserTransaction basicUserTransaction = new BasicUserTransaction();
                basicUserTransaction.PayIn();
                return true;
            }
            else if (result == "3")
            {
                BasicUserTransaction basicUserTransaction = new BasicUserTransaction();
                basicUserTransaction.WithDraw();
                return true;
            }
            else if (result == "4")
            {
                BasicUserTransaction basicUserTransaction = new BasicUserTransaction();
                basicUserTransaction.CheckAcountBalance();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
