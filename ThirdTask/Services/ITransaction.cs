﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThirdTask.Services
{
    interface ITransaction
    {
        void PayIn();
        void WithDraw();
        void CheckAcountBalance();

        void Transfer();
    }
}
